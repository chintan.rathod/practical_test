package com.chintan.test

import com.chintan.test.network.ApiService
import com.chintan.test.network.domain.response.TrackResponse
import com.chintan.test.repository.NetworkRepository
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class NetworkRepositoryTest
{
    lateinit var networkRepository: NetworkRepository

    @Mock
    lateinit var apiService: ApiService

    @Before
    fun setup()
    {
        MockitoAnnotations.initMocks(this)
        networkRepository = NetworkRepository(apiService)
    }

    @Test
    fun `search track with blank test`() {
        runBlocking {
            Mockito.`when`(apiService.searchTracks("")).thenReturn(null)
            val response = networkRepository.searchTracks("")
            assertEquals(null, response)
        }
    }

    @Test
    fun `search track with sample text test`() {
        runBlocking {
            val mockResponse = mock(TrackResponse::class.java)
            Mockito.`when`(apiService.searchTracks("love")).thenReturn(mockResponse)
            val response = networkRepository.searchTracks("love")
            assertEquals(mockResponse, response)
        }
    }
}