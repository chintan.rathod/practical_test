package com.chintan.test

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chintan.test.network.ApiService
import com.chintan.test.network.domain.response.TrackResponse
import com.chintan.test.repository.NetworkRepository
import com.chintan.test.ui.viewmodel.TracksViewModel
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class PostViewModelTest
{
    private val testDispatcher = TestCoroutineDispatcher()
    lateinit var postsViewModel: TracksViewModel

    @Mock
    lateinit var networkRepository: NetworkRepository

    @Mock
    lateinit var apiService: ApiService

    @get:Rule
    val instantTaskExecutionRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        networkRepository = NetworkRepository(apiService)
        postsViewModel = TracksViewModel(networkRepository)
    }

    @Test
    fun searchTrack() {
        runBlocking {
            val mockResponse = mock(TrackResponse::class.java)
            Mockito.`when`(networkRepository.searchTracks("love"))
                .thenReturn(mockResponse)

            var result = postsViewModel.searchTracks("love")
            assertEquals(mockResponse, result)
        }
    }
}