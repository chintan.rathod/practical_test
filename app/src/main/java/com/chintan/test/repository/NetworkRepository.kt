package com.chintan.test.repository

import com.chintan.test.network.ApiService
import com.chintan.test.network.domain.Track
import com.chintan.test.network.domain.response.TrackInfoResponse
import com.chintan.test.network.domain.response.TrackResponse
import javax.inject.Inject

/**
 * This repository is used to implement API service calls.
 * This provides an abstraction layer.
 */
class NetworkRepository @Inject constructor
    (private val apiService : ApiService)
{
    suspend fun searchTracks(track : String): TrackResponse
    {
        return apiService.searchTracks(track)
    }

    suspend fun getTrackInfo(track : String,
                             artist : String): TrackInfoResponse
    {
        return apiService.getTrackInfo(track, artist)
    }
}