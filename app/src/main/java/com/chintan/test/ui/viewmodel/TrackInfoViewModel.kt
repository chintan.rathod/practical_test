package com.chintan.test.ui.viewmodel

import androidx.lifecycle.*
import com.chintan.test.network.domain.response.TrackInfoResponse
import com.chintan.test.repository.NetworkRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TrackInfoViewModel @Inject constructor(
    private val networkRepository: NetworkRepository
) : ViewModel()
{
    // Create a LiveData with a TrackResponse
    private val _result: MutableLiveData<TrackInfoResponse> by lazy {
        MutableLiveData<TrackInfoResponse>()
    }

    // Create a LiveData with a String
    private val error: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    /*
    Remote method call for Track Info
     */
    fun getTrackInfo(track: String, artist: String)
    {
        try
        {
            viewModelScope.launch(Dispatchers.IO) {
                val response = networkRepository.getTrackInfo(track, artist)
                _result.postValue(response)
            }
        }
        catch (exception: Exception)
        {
            error.postValue(exception.message)
        }
    }

    fun getResult(): LiveData<TrackInfoResponse>
    {
        return _result
    }

    fun getError(): LiveData<String>
    {
        return error
    }

}