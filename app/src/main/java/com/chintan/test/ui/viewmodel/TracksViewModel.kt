package com.chintan.test.ui.viewmodel

import androidx.lifecycle.*
import com.chintan.test.network.domain.response.TrackResponse
import com.chintan.test.repository.NetworkRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TracksViewModel @Inject constructor(
    private val networkRepository: NetworkRepository
) : ViewModel()
{
    // Create a LiveData with a TrackResponse
    private val result: MutableLiveData<TrackResponse> by lazy {
        MutableLiveData<TrackResponse>()
    }

    // Create a LiveData with a String
    private val error: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    /*
    Remote method call for Posts
     */
    fun searchTracks(track: String)
    {
        try
        {
            viewModelScope.launch(Dispatchers.IO) {
                val response = networkRepository.searchTracks(track)
                result.postValue(response)
            }
        }
        catch (exception: Exception)
        {
            error.postValue(exception.message)
        }
    }

    fun getResult(): LiveData<TrackResponse>
    {
        return result
    }

    fun getError(): LiveData<String>
    {
        return error
    }
}