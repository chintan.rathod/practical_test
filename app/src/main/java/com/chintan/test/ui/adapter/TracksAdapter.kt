package com.chintan.test.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chintan.test.R
import com.chintan.test.databinding.ItemLayoutBinding
import com.chintan.test.network.domain.Track
import com.chintan.test.ui.view.TrackInfoActivity

/**
 * This adapter class will be used to show list of tracks in tracks activity.
 * This will take "item_layout" to render the view in linear fashion.
 */
class TracksAdapter(private val tracks : ArrayList<Track>) : RecyclerView.Adapter<TracksAdapter.DataViewHolder>()
{
    class DataViewHolder(private val binding: ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind(track: Track)
        {
            if (track.image != null && track.image.isNotEmpty())
            {
                Glide.with(binding.image.context)
                    .load(track.image[0].text)
                    .into(binding.image)
            }
            else
            {
                Glide.with(binding.image.context)
                    .load(R.drawable.ic_launcher_foreground)
                    .into(binding.image)
            }
            binding.title.text= track.name;
            binding.body.text= track.artist

            /*
            It will apply click listener to the root layout of item_layout
            and when clicked it will open detailed view of the track
             */
            binding.root.setOnClickListener {
                val intent = Intent(binding.root.context, TrackInfoActivity::class.java)
                intent.putExtra("track", track.name)
                intent.putExtra("artist", track.artist)
                binding.root.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : DataViewHolder {
        val binding = ItemLayoutBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return DataViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) = holder.bind(tracks[position])

    override fun getItemCount(): Int
    {
        return tracks.size
    }

    fun addData(list : List<Track>)
    {
        this.tracks.clear()
        this.tracks.addAll(list)
    }
}