package com.chintan.test.ui.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.chintan.test.R
import com.chintan.test.databinding.ActivityTrackInfoBinding
import com.chintan.test.network.domain.response.TrackInfoResponse
import com.chintan.test.ui.viewmodel.TrackInfoViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TrackInfoActivity : AppCompatActivity()
{
    /*
    ViewModel to interact with repository
     */
    private val viewModel: TrackInfoViewModel by viewModels()

    /*
    This object is used to identify view binding of layout
     */
    private lateinit var binding : ActivityTrackInfoBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityTrackInfoBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

//        binding.viewmodel = viewModel

        setView()
        setObservers()
    }

    /*
    Initial method to set all views of layout
     */
    private fun setView()
    {
        val trackName = intent.getStringExtra("track")
        val artistName = intent.getStringExtra("artist")
        showProgress(true)
        viewModel.getTrackInfo(trackName!!, artistName!!)
    }

    /*
    Set observer
     */
    private fun setObservers()
    {
        viewModel.getResult().observe(this, Observer { it ->
            setData(it)
        })

        viewModel.getError().observe(this, Observer { it ->
            showError(it)
        })
    }

    private fun showProgress(show: Boolean)
    {
        if (show)
        {
            binding.progress.visibility = View.VISIBLE
        }
        else
        {
            binding.progress.visibility = View.GONE
        }
    }

    private fun showError(message: String?)
    {
        showProgress(false)
        binding.progress.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun setData(it: TrackInfoResponse)
    {
        showProgress(false)
        binding.title.text = it.track.name
        binding.listeners.text = String.format(getString(R.string.listeners), it.track.listeners)
        binding.play.text = String.format(getString(R.string.total_play), it.track.playcount)
        binding.artistName.text = it.track.artist.name

        if (it.track.album.image.size > 2)
        {
            Glide.with(binding.image.context)
                .load(it.track.album.image[2].text)
                .into(binding.image)
        }

        if (it.track.toptags.tag.size > 4)
        {
            binding.tag1.text = it.track.toptags.tag[0].name
            binding.tag2.text = it.track.toptags.tag[1].name
            binding.tag3.text = it.track.toptags.tag[2].name
            binding.tag4.text = it.track.toptags.tag[3].name
            binding.tag5.text = it.track.toptags.tag[4].name
        }
        else
        {
            binding.flow.visibility = View.GONE
        }
    }
}