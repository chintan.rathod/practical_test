package com.chintan.test.ui.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.chintan.test.databinding.ActivityMainBinding
import com.chintan.test.network.domain.Track
import com.chintan.test.network.domain.response.TrackResponse
import com.chintan.test.ui.adapter.TracksAdapter
import com.chintan.test.ui.viewmodel.TracksViewModel
import com.jakewharton.rxbinding.widget.RxTextView
import dagger.hilt.android.AndroidEntryPoint
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class TracksActivity : AppCompatActivity()
{
    /*
    ViewModel to interact with repository
     */
    private val viewModel: TracksViewModel by viewModels()

    /*
    Tracks list adapter
     */
    private lateinit var adapter: TracksAdapter

    /*
    This object is used to identify view binding of layout
     */
    lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setView()
        setObservers()
    }

    /*
    Initial method to set all views of layout
     */
    private fun setView()
    {
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = TracksAdapter(arrayListOf())
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(this,
            (binding.recyclerView.layoutManager as LinearLayoutManager).orientation
        )
        )
        binding.recyclerView.adapter = adapter
    }

    /*
    Set observer of posts
     */
    private fun setObservers()
    {
        viewModel.getResult().observe(this, Observer { it ->
            setData(it);
        })

        viewModel.getError().observe(this, Observer { it ->
            showError(it)
        })

        /*
        Below code will wait for user to stop typing and it will execute API call
        after 500 ms delayed stop typing.
         */
        RxTextView.textChanges(binding.editSearch)
            .debounce(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .subscribe { textChanged ->
                showProgress()
                viewModel.searchTracks(textChanged.toString())
            }
    }

    private fun showProgress()
    {
        binding.messageNoItems.visibility = View.GONE
        binding.progress.visibility = View.VISIBLE
        binding.recyclerView.visibility = View.GONE
    }

    private fun showError(message: String?)
    {
        binding.messageNoItems.visibility = View.VISIBLE
        binding.progress.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun setData(it: TrackResponse?)
    {
        binding.messageNoItems.visibility = View.GONE
        binding.progress.visibility = View.GONE
        binding.recyclerView.visibility = View.VISIBLE
        it?.results?.let { tracks -> renderList(tracks.trackMatches.track) }
    }

    /*
    Once list is populated, this method will be called from observer
     */
    private fun renderList(tracks: List<Track>)
    {
        adapter.addData(tracks)
        adapter.notifyDataSetChanged()

        /*
        Check if list is emply, then show a message to user to try for new search word
         */
        if (tracks.isEmpty())
        {
            binding.messageNoItems.visibility = View.VISIBLE
        }
    }

//    fun View.findViewTreeLifecycleOwner(): LifecycleOwner? = ViewTreeLifecycleOwner.get(this)
}