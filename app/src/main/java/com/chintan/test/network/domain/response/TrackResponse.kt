package com.chintan.test.network.domain.response

import com.chintan.test.network.domain.Results

data class TrackResponse(
    val results: Results
)