package com.chintan.test.network.domain

data class Artist(
    val mbid: String,
    val name: String,
    val url: String
)