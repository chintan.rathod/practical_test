package com.chintan.test.network.domain

data class Trackmatches(
    val track: List<Track>
)