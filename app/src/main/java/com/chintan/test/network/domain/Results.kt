package com.chintan.test.network.domain

import com.google.gson.annotations.SerializedName

data class Results(
    @SerializedName("opensearch:Query")
    val openSearchQuery: OpensearchQuery,

    @SerializedName("opensearch:itemsPerPage")
    val itemsPerPage: String,

    @SerializedName("opensearch:startIndex")
    val startIndex: String,

    @SerializedName("opensearch:totalResults")
    val totalResults: String,

    @SerializedName("trackmatches")
    val trackMatches: Trackmatches
)