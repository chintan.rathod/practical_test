package com.chintan.test.network.domain

/**
 * This enum class is used in #Resource data class.
 * Indicates
 * Success = Operation Successful with Response
 * Error = Operation failed with error message
 * Loading = Current operation is in process
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}