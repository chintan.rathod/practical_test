package com.chintan.test.network.domain

data class Album(
    val artist: String,
    val image: List<Image>,
    val mbid: String,
    val title: String,
    val url: String
)