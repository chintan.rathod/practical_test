package com.chintan.test.network.domain

/**
 * This data class hold information of Post
 */
data class Track(
    val artist: String,
    val image: List<Image>,
    val listeners: String,
    val mbid: String,
    val name: String,
    val streamable: String,
    val url: String
)