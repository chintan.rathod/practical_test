package com.chintan.test.network.domain

data class OpensearchQuery(
    val text: String,
    val role: String,
    val startPage: String
)