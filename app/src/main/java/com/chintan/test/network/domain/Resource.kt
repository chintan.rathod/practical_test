package com.chintan.test.network.domain

/**
 * This data class is generic class used for each response to ViewModel.
 * This will be used in the view to identify current status of operation.
 */
data class Resource<out T>(val status: Status, val data: T?, val message: String?)
{
    companion object
    {
        fun <T> success(data: T?) : Resource<T>
        {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(data: T?, message: String?): Resource<T>
        {
            return Resource(Status.ERROR, data, message)
        }

        fun <T> loading(data: T?) : Resource<T>
        {
            return Resource(Status.LOADING, data, null)
        }
    }
}
