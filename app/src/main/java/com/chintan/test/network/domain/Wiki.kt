package com.chintan.test.network.domain

data class Wiki(
    val content: String,
    val published: String,
    val summary: String
)