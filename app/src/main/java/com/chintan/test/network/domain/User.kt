package com.chintan.test.network.domain

import com.google.gson.annotations.SerializedName

/**
 * This data class hold information of user
 */
data class User (
    @SerializedName("id")
    val id : Int = 0,

    @SerializedName("name")
    val name : String = "",

    @SerializedName("username")
    val username : String = ""
)