package com.chintan.test.network

import com.chintan.test.network.domain.response.TrackInfoResponse
import com.chintan.test.network.domain.response.TrackResponse

import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

/**
 * This class is used to make network calls
 */
interface ApiService {

    @Headers(
        "Accept: application/json",
        "Content-Type: application/json")
    @GET("?method=track.search&api_key=897926e36a346151fbf9a0c23fe70a58&format=json")
    suspend fun searchTracks(@Query("track") track : String): TrackResponse

    @Headers(
        "Accept: application/json",
        "Content-Type: application/json")
    @GET("?method=track.getInfo&api_key=897926e36a346151fbf9a0c23fe70a58&format=json")
    suspend fun getTrackInfo(@Query("track") track : String,
                             @Query("artist") artist : String): TrackInfoResponse
}