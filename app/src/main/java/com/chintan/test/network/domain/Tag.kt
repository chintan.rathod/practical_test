package com.chintan.test.network.domain

data class Tag(
    val name: String,
    val url: String
)