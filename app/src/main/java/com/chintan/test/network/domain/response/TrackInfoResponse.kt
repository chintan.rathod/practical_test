package com.chintan.test.network.domain.response

import com.chintan.test.network.domain.TrackX

data class TrackInfoResponse(
    public val track: TrackX
)